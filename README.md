[^_^]: # -----------------------------------------------------------------------
[^_^]: # Carpe Diem
[^_^]: #
[^_^]: # File Name: README.mdtest.md
[^_^]: # Author: xinge
[^_^]: # Mail: 1391533278@qq.com
[^_^]: # Created Time: 2018-07-20 02:50:01
[^_^]: # Last modified: 2020-02-22 20:15:48
[^_^]: # Description:一个简单的 \LaTeX{} 文档
[^_^]: # -----------------------------------------------------------------------

# 恭喜你，已经成功创建\LaTeX{} 项目：my_latex

## 目录结构如下：

```
my_latex
├── 00-main.tex
├── 01-preamble.tex
├── 02-cover.tex
├── README.md
├── chapter
│   ├── chapter-01.tex
│   ├── chapter-02.tex
│   └── chapter-03.tex
├── png
│   └── logo.png
└── reference
    └── reference.bib
```

## 1. 下面我们详细来介绍目录及目录中文件的用途。

1. 00-main.tex 是这个项目的主文件，选用哪个模板，指定引言文件，选择封面文件，编译哪些章节，都将在这个主文件中完成。

2. 01-preamble.tex 是引言文件，页面设置，字体，版面，参考文献数据库文件的路径及名称在这里完成设置。

3. 02-cover.tex 是封面文件，您可以随意修改，或是下载其他的模板替换这个文件都可以。

4. chapter 目录，是存放具体文章的地方，每个章节一个文档，方便编辑，也便于编译检查。

5. png 目录，存放文档需要的图片，便于文中引用。

6. reference 目录，参考文献数据库存放在这个目录中。

## 2. 编译命令

1. 开始编译 pdf: leader+ll [两个小写的 L]

2. 暂停编译 pdf: leader+ll [两个小写的 L]

3. 终止编译 pdf: leader+lk [小写的 L 和小写的 K]

4. 跳到 pdf 文件: leader+lv [小写的 L 和小写的 V]

5. 跳回 tex 文件: Command + Shift + 单击

6. 清除 tmp 文件: leader+lc [小写的 L 和小写的 C]

7. 清除 pdf 文件: leader+lC [小写的 L 和大写的 C]

## 3. 参考文献数据库文件

关于参考文献，我们选用 bibtex 格式，Google scholar(Google 学术)是支持直接导出的，方法如下：

1. 打开 [Google scholar Google 学术](https://scholar.google.com/)

2. 点击左上角 ≡，在设置--参考书目管理软件--显示导入 BibTeX 的链接，然后保存。

3. 再次搜索书目，书籍名称下方会出现 导入 BibTex，直接点击链接，全部复制-粘贴到当前目录的 reference/reference.bib 中。可以参考 reference.bib 文件中的格式。
